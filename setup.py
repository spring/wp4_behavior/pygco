# This file, part of PyGCO in the WP6 of the Spring project,
# is part of a project that has received funding from the
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
#
# Copyright (C) 2020-2024 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import setuptools
import subprocess
from setuptools.command.build_ext import build_ext
import os
from shutil import copyfile


BASEPATH = os.path.dirname(os.path.abspath(__file__))
PYGCOPATH = os.path.join(BASEPATH, 'pygco')
GCOURL = "https://vision.cs.uwaterloo.ca/files/gco-v3.0.zip"


def get_gco():
    cmd = ['wget', GCOURL]
    subprocess.call(cmd, cwd=os.path.join(PYGCOPATH))
    cmd = ['unzip', os.path.join(PYGCOPATH, '*.zip'), '-d', os.path.join(PYGCOPATH, "gco_source")]
    subprocess.call(cmd, cwd=os.path.join(PYGCOPATH))



class Build_ext(build_ext):
    """Customized setuptools build command - builds protos on build."""

    def run(self):

        # download and compile the underlying c++ library to get the libcgco.so
        get_gco()
        cmd = ['make']
        subprocess.call(cmd, cwd=PYGCOPATH)
        build_ext.run(self)


with open(BASEPATH + "/README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name='pygco',
    version='0.1.0',
    packages=['pygco'],
    package_dir={'pygco': 'pygco'},
    package_data={'pygco': ['libcgco.so']},
    include_package_data=True,
    cmdclass={'build_ext': Build_ext},
    install_requires=[
        'numpy',
    ])
