# PyGCO

Graph cuts in python.
Using GCO library by Olga Veksler.
Based on python interface by Yujia Li.

# Installation

In the repository, run

```
python setup.py build_ext && python setup.py install
```
